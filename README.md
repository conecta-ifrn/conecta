# Conecta!

## Participantes
<ol>
    <li>Bruna Borges</li>
    <li>Diogo Henrique</li>
    <li>Gabriel Fernandes</li>
    <li>Letícia Ribeiro</li>
    <li>Samuel Oliveira</li>
</ol>

## Sobre o Projeto

<p>Este projeto visa abordar um desafio crítico que afeta alunos e professores dos Institutos Federais (IFs) em todo o país: a falta de informações claras e acessíveis sobre oportunidades de pesquisa, extensão e bolsas acadêmicas. A ausência de dados precisos e a falta de divulgação adequada tornam a entrada e participação em projetos acadêmicos um processo complexo e desafiador.</p>
<br>
<p>Nossa missão é criar um sistema de gerenciamento de informações que forneça aos alunos e coordenadores de projetos um local centralizado para acessar detalhes sobre oportunidades acadêmicas. Este sistema promoverá a transparência, facilitando a busca por projetos, critérios de seleção e prazos importantes.</p>
<br>
<p>Além disso, pretendemos coletar feedback dos participantes para melhorar continuamente a experiência acadêmica. Com informações mais acessíveis e um processo simplificado, esperamos aumentar a participação em projetos de pesquisa e extensão, contribuindo para o desenvolvimento acadêmico e profissional dos alunos e fortalecendo a pesquisa nos IFs.</p>
<br>
<p>Nossa abordagem incluirá a validação de suposições e a investigação das necessidades reais dos alunos e coordenadores. Trabalharemos em estreita colaboração com a comunidade acadêmica para garantir que o sistema atenda às expectativas e promova uma participação equitativa.</p>
<br>
<p>Este projeto tem o potencial de tornar a experiência acadêmica nos IFs mais acessível, inclusiva e rica em oportunidades. Acreditamos que ao enfrentar a falta de informações, podemos abrir portas para uma educação mais enriquecedora e colaborativa em instituições de ensino superior em todo o Brasil.</p>

## Licença

Este projeto é licenciado sob os termos da Licença Pública Geral GNU, versão 3 (GPLv3).

A GPLv3 é uma licença de software livre que protege as liberdades dos usuários de software livre. Isso significa que você tem o direito de usar, estudar, modificar e distribuir este software de acordo com os termos da GPLv3.

### Detalhes da Licença

Para ler a licença completa, consulte o arquivo [LICENSE] https://www.gnu.org/licenses/gpl-3.0.txt

Aqui está um resumo dos principais pontos da GPLv3:

- Você tem o direito de usar este software para qualquer finalidade, seja pessoal, acadêmica ou comercial.
- Você pode estudar o código-fonte, fazer modificações e distribuir suas próprias versões.
- Se você distribuir este software, deve fornecer o código-fonte e manter a licença intacta.
- Não é permitido adicionar restrições adicionais aos usuários deste software.

Lembre-se de que ao usar este software, você concorda em cumprir os termos da GPLv3. Se você tiver dúvidas ou precisar de mais informações sobre a licença, consulte o arquivo [LICENSE] https://www.gnu.org/licenses/gpl-3.0.txt para obter os detalhes completos
